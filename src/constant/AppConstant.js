const backgroundColor = '#2e2e2e'
const activeTintColor = '#c0022a'
const inactiveTintColor = '#fff'
const apiHost = '192.168.0.104'
const apiPort = '3334'
export {
    backgroundColor,
    activeTintColor,
    inactiveTintColor,
    apiHost,
    apiPort
}