import { 
    OPERATION_PROPS,
    ACTIVE_OPERATION_PROPS,
    INCREASE_RANK,
    RANK,
    CLEAR_RANK,
    SCREEN_PROPS
} from "../actions/type";

const INITIAL_STATE = {
    company: null,
    operation: null,
    newRank: {},
    rank: {},
    screen: ''
}

export default(state = INITIAL_STATE, actions) => {
    switch (actions.type) {
        case OPERATION_PROPS:
            return {...state, company: actions.payload};
        case ACTIVE_OPERATION_PROPS:
            return {...state, operation: actions.payload};
        case INCREASE_RANK:
            return {...state, newRank: actions.payload};
        case CLEAR_RANK:
            return {...state, newRank: {}, rank: {}};
        case RANK:
            return {...state, rank: actions.payload};
        case SCREEN_PROPS:
            return {...state, screen: actions.payload};
        default:
            return state;
    }
}