import {
    PUSH_DATA,
    SCAN_START,
    SCAN_STOP,
    GET_BEACON_DATA,
    PUSH_COMPANY_ID,
} from '../actions/type';

const INITIAL_STATE = {
    companies: [], // Dönen şirket bilgilerinin obje halinde bulunduğu array
    operations: [],
    scanning: false,
}


export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case PUSH_DATA:
            return{
                ...state
            }
        
        case SCAN_START:
            return{
                ...state,
                scanning: true
            }

        case SCAN_STOP:
            return{
                ...state,
                scanning: false
            }
        
        case GET_BEACON_DATA:
            return{
                ...state,
                companies: action.payload.companies,
                operations: action.payload.operations
            }
        
        case PUSH_COMPANY_ID:
            return{
                ...state
            }
        
        default:
            return state;
    }
}
