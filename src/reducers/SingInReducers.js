import { 
    LOGIN_TEXT_CHANGE,
    SUCCESS_USER
} from "../actions/type";
const INITIAL_STATE = {
    tc: '',
    password: '',
    user: {}
};

export default (state = INITIAL_STATE, actions) => {
    switch(actions.type){
        case LOGIN_TEXT_CHANGE:
            return {...state, [actions.payload.props]:actions.payload.value}
        case SUCCESS_USER:        
            return {...state, user: actions.payload}
        default: 
            return state;
    }
}