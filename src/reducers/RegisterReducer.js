import { 
    REGISTER_TEXT_CHANGE
} from "../actions/type";

const INITIAL_STATE = {
    tc : '',
    name : '',
    surname : '',
    day : '',
    mount : '',
    year : '',
    phone : '',
    email : '',
    password : '',
    passwordAgain : ''
}

export default(state = INITIAL_STATE, actions) => {
    switch (actions.type) {
        case REGISTER_TEXT_CHANGE:
            return {...state, [actions.payload.props]: actions.payload.value}
        default:
            return state
    }
}