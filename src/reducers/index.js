import { combineReducers } from "redux";
import SingInReducers from "./SingInReducers";
import RegisterReducer from "./RegisterReducer";
import SearchReducers from "./SearchReducers";
import OperationReducer from "./OperationReducer";
import RankReducers from "./RankReducers";

export default combineReducers({
    singInResponse: SingInReducers,
    registerResponse: RegisterReducer,
    searchResponse: SearchReducers,
    operationResponse: OperationReducer,
    rankResponse: RankReducers
});