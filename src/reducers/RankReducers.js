import { 
    ALL_RANK,
    REFRESH_START,
    REFRESH_STOP
} from "../actions/type";

const INITIAL_STATE = {
    rank: [],
    refreshing: false
}

export default(state = INITIAL_STATE, actions) => {
    switch (actions.type) {
        case ALL_RANK:
            return {...state, rank: actions.payload}
        case REFRESH_START:
            return {...state, refreshing: true}
        case REFRESH_STOP:
            return {...state, refreshing: false}
        default:
            return state
    }
}