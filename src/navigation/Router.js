import React from "react";
import { Text } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { createSwitchNavigator, createStackNavigator, createBottomTabNavigator } from "react-navigation";

import AuthLoadingScreen from "../screen/AuthLoading/AuthLoadingScreen";
import SignInScreen from "../screen/Auth/SignInScreen";
import SettingsScreen from "../screen/App/SettingsScreen";
import RankScreen from "../screen/App/RankScreen";
import SearchScreen from "../screen/App/SearchScreen";
import RegisterScreen from "../screen/Auth/RegisterScreen";
import ForgetPasswordScreen from "../screen/Auth/ForgetPasswordScreen";
import VerifyCodeScreen from "../screen/Auth/VerifyCodeScreen";
import PasswordChangeScreen from "../screen/Auth/PasswordChangeScreen";
import TicketPrint from "../screen/TicketPrint";
import OperationScreen from "../screen/App/OperationScreen";

import {
  backgroundColor,
  inactiveTintColor,
  activeTintColor 
} from "../constant/AppConstant";

// Tab Navigator
const RankStack = createStackNavigator({
  Rank: RankScreen,
  RTicket: TicketPrint
},{
  headerMode: 'none'
});
const SearchStack = createStackNavigator({
  Search: SearchScreen,
  Operation: OperationScreen,
  Ticket: TicketPrint 
},{
  headerMode: 'none'
})
const SettingsStack = createStackNavigator({ 
  Settings: SettingsScreen, 
},{
  headerMode: 'none'
});

const AppTab = createBottomTabNavigator({
  TRank: {screen: RankStack},
  TSearch: {screen: SearchStack},
  TSettings: {screen: SettingsStack}
},{
  navigationOptions: ({ navigation }) =>({
    tabBarLabel: ({focused,tintColor}) => {
      const {routeName} = navigation.state
      let label
      if(routeName == 'TRank'){
        label = 'Sıralar'
      }else if(routeName == 'TSearch'){
        label = 'Yakındaki İşletmeler'
      }else if(routeName == 'TSettings'){
        label = 'Ayarlar'
      }
      return <Text style={{textAlign: 'center', color: focused ? tintColor:null}}>{label}</Text>
    },
    tabBarIcon: ({focused, tintColor}) => {
      const {routeName} = navigation.state
      let icon
      if(routeName == 'TRank'){
        icon = 'md-list'
      }else if(routeName == 'TSearch'){
        icon = 'ios-search'
      }else if(routeName == 'TSettings'){
        icon = 'ios-settings'
      }
      return <Ionicons name={icon} color={tintColor} size={30}/>
    }
  }),
  tabBarOptions: {
    showLabel: false,
    activeTintColor: activeTintColor,
    inactiveTintColor: inactiveTintColor,
    tabStyle: {
      backgroundColor: backgroundColor
    }
  },
  animationEnabled: true,
  swipeEnabled: true,
  tabBarPosition: 'bottom',
})
//Switch Navigator
const AuthStack = createStackNavigator({ 
  SignIn: SignInScreen,
  Register: RegisterScreen,
  ForgetPassword: ForgetPasswordScreen,
  VerifyCode: VerifyCodeScreen,
  PasswordChange: PasswordChangeScreen
},{
  headerMode: 'none'
})
const AuthLoadingStack = createStackNavigator({
  SAuthLoading: AuthLoadingScreen
},{
  headerMode: 'none'
})

export default createSwitchNavigator(
{
  AuthLoading: AuthLoadingStack,
  App: AppTab,
  Auth: AuthStack,
},{
  initialRouteName: 'AuthLoading'
}
);