import React from "react";
import { TouchableOpacity, Text } from "react-native";
import PropTypes from 'prop-types'
import { operationProps } from "../actions";
import { connect } from "react-redux";

type Props ={
    name: string, 
    item: object, 
    navigation: object
}
class CompanyListItem extends React.Component<Props> {
    _onPress = () => {
        this.props.operationProps(this.props.item)
        this.props.navigation.navigate('Operation');
    }
    render(){
        return (
            <TouchableOpacity onPress={this._onPress} activeOpacity={0.5} style={{width: '95%', minHeight: 50, justifyContent: 'center', marginHorizontal: 5, alignSelf: 'center', backgroundColor: '#ff4d4d', paddingHorizontal: 10, marginVertical: 10, borderRadius: 8}}>
                <Text numberOfLines= {1} style={{fontSize :22, color: '#fff'}}>{this.props.name}</Text>
            </TouchableOpacity>
        )
    }
}
const mapStateToProps = () => {
    return {

    }
}
export default connect(mapStateToProps,{
    operationProps
})(CompanyListItem);