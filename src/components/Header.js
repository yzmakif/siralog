import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const Header = ({Left,title,Rigth, backgroundColor, leftPress}) => {
    return(
        <View 
            style={{
                width: '100%',
                height: 55,
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: backgroundColor ? backgroundColor: '#1F4294',
                elevation: 5,
                paddingRight: 15
            }}
        >
            {
                Left ?(
                    <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center', height: '100%', width: 60}} onPress={leftPress}>
                        {Left}
                    </TouchableOpacity>
                ):null
            }
            <View style={{flex: 1}}>
                <Text numberOfLines={1} style={{fontSize: 22, color: '#fff',marginLeft: Left ? 0:15 }}>{title}</Text>
            </View>
        </View>
    )
}
export default Header;