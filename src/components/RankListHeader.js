import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class RankListHeader extends Component {
  render() {
    const {
        title
    } = this.props;
    return (
      <View style={{width: '100%', height: 45, backgroundColor: '#ff4d4d', justifyContent: 'center',alignItems: 'center',marginBottom: 3}}>
        <Text style={{fontSize: 20, fontWeight: '700', color: '#fff'}}>{title}</Text>
      </View>
    )
  }
}
export default RankListHeader