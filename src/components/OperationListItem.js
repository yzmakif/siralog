import React from "react";
import { TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";

import { newRank, screenProps } from "../actions";

type Props ={
    name: string, 
    item: object, 
    navigation: object,
    companyId: Number,
    userId: Number
}
class OperationListItem extends React.Component<Props> {
    _onPress = () => {
        const {
            userId, 
            companyId,
            navigation,
            item
        } = this.props
        this.props.screenProps('newRank')
        this.props.newRank(navigation,userId, companyId, item.id)
    }
    render(){
        return (
            <TouchableOpacity onPress={this._onPress} activeOpacity={0.5} style={{width: '95%', height: 50, justifyContent: 'center', marginHorizontal: 5, alignSelf: 'center', backgroundColor: '#ff4d4d', paddingHorizontal: 10, marginVertical: 10, borderRadius: 8}}>
                <Text style={{fontSize :22, color: '#fff'}}>{this.props.name}</Text>
            </TouchableOpacity>
        )
    }
}
const mapStateToProps = () => {
    return {

    }
}
export default connect(mapStateToProps,{
    newRank,
    screenProps
})(OperationListItem);