import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import Ionicons from "react-native-vector-icons/Ionicons";
import { connect } from "react-redux";

import { changeRank, screenProps } from "../actions";
export class RankListItem extends Component {

    _onPress = () => {
        this.props.changeRank(this.props.item)
        this.props.screenProps('rank')
        this.props.navigation.navigate('RTicket')
    }
    render() {
        const {
            item
        } = this.props
        
        return (
            <TouchableOpacity onPress={this._onPress} activeOpacity={0.5} style={{width: '99%', height: 150, backgroundColor: '#2e2e2e', marginBottom: 3, alignSelf: 'center', flexDirection: 'row'}}>
                <View style={{flex: 5}}>
                    <View style={{flex: 1, justifyContent: 'center', paddingLeft: 15}}>
                        <View style={{flex: 1, justifyContent: 'center'}}>
                            <Text numberOfLines={1} style={{fontSize : 18, fontWeight: '500', color: '#fff'}}>{item.companyName}</Text>
                        </View>
                        <View style={{flex: 1, justifyContent: 'center'}}>
                            <Text numberOfLines={1} style={{fontSize : 17, fontWeight: '500', color: '#fff'}}>{item.operationName}</Text>
                        </View>
                    </View>
                    <View style={{flex: 1, justifyContent: 'center', paddingLeft: 15}}>
                        <View style={{flex: 1, justifyContent: 'center'}}>
                            <Text numberOfLines={1} style={{fontSize : 15, fontWeight: '500', color: '#fff'}}>{item.date.slice(0,10)}</Text>
                        </View>
                        <View style={{flex: 1, justifyContent: 'center'}}>
                            <Text numberOfLines={1} style={{fontSize : 15, fontWeight: '500', color: '#fff'}}>{item.date.slice(11,16)}</Text>
                        </View>
                    </View>
                </View>
                <View style={{flex: 2, justifyContent: 'center', alignItems: 'center'}}>
                    <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', width: 60, height: 60, borderRadius: 60, borderWidth: 3, borderColor: '#fff'}}>
                        <Ionicons name='md-person' size={20} color='#fff'/>
                        <Text style={{fontSize: 20, color: '#fff', marginLeft: 3}}>{item.number}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}
const mapStateToProps = () => {
    return {

    }
}
export default connect(mapStateToProps, {
    changeRank,
    screenProps
})(RankListItem)