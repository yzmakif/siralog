import React, { Component } from 'react';
import { View, Text, TextInput, ViewStyle} from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";

type Props = {
  defaultValue: string,
  value: string,
  placeholder: string,
  autoCorrect: bool,
  autoFocus: bool,
  editable: bool,
  keyboardType: string,
  maxLength: number,
  multiline: number,
  onChangeText: func,
  onFocus: func,
  onBlur: func,
  secureTextEntry: bool,
  selectTextOnFocus: bool,
  returnKeyType: string,
  onSubmitEditing: func,
  width: number,
  style: ViewStyle,
  height: number,
  iconName: string,
  label: bool,
  onRef: func
};
class TextInputComponent extends Component<Props> {
  state={
    focus: false
  }
  _onFocus = () => {
    this.setState({focus: true})
  }
  _onBlur = () => {
    this.setState({focus: false})
  }
  _onSubmitEditing = () => {
    this.props.onSubmitEditing ? this.props.onSubmitEditing():null
  }
  render(){
  const height = typeof this.props.height ==='undefined' ? 55:this.props.height;
  return(
    <View style={[{width : this.props.width ? this.props.width:'100%', height: height,backgroundColor:'#ede9e8'}, this.props.style]}>
      {
        this.props.label 
        ? 
        <View style={{width : this.props.width ? this.props.width:'100%', height: 15, marginLeft: (height/2)-15}}>
          <Text style={{fontSize: 12}}>
            {this.props.placeholder}
          </Text>
        </View>
        :
        null
      }
      <View style={{flexDirection: 'row',width : this.props.width ? this.props.width:'100%', height:this.props.label ? height-15:height}}>
        {
          this.props.iconName ? (
            <View style={{width: height, height: this.props.label ? height-15:height, justifyContent: 'center', alignItems: 'center'}}>
              <Ionicons name={this.props.iconName} size={30} color={this.state.focus ? '#2e2e2e':'#BDB5B5'}/>
            </View>
          ):null
        } 
        <TextInput
          ref={ref => this.props.onRef(ref)}
          style={{flex:1, fontSize: 17, marginRight: 15}}
          underlineColorAndroid={'transparent'}
          defaultValue={this.props.defaultValue}
          value={this.props.value}
          placeholder={this.props.placeholder}
          autoCorrect={this.props.autoCorrect}
          autoFocus={this.props.autoFocus}
          editable={typeof this.props.editable == 'undefined'? true : this.props.editable}
          keyboardType={this.props.keyboardType}
          maxLength={this.props.maxLength}
          multiline={this.props.multiline}
          onSubmitEditing={this._onSubmitEditing}
          onChangeText={this.props.onChangeText}
          onFocus={this.props.onFocus ? this.props.onFocus : this._onFocus}
          onBlur={this.props.onBlur ? this.props.onBlur:this._onBlur}
          secureTextEntry={this.props.secureTextEntry}
          selectTextOnFocus={this.props.selectTextOnFocus}
          returnKeyType={this.props.returnKeyType}
        />
      </View>
    </View>
  );
  }
};
export default TextInputComponent;