import React, { Component } from 'react';
import { View, TextInput, ViewStyle, TextStyle} from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
type Props = {
  value: string,
  defaultValue: string,
  placeholder: string,
  autoCorrect: bool,
  autoFocus: bool,
  editable: bool,
  keyboardType: string,
  maxLength: number,
  multiline: number,
  onChangeText: func,
  onFocus: func,
  onBlur: func,
  secureTextEntry: bool,
  selectTextOnFocus: bool,
  returnKeyType: string,
  onSubmitEditing: func,
  conteinerStyle: ViewStyle,
  height: number,
  iconName: string,
  iconColor: string,
  activeIconColor: string,
  inactiveIconColor: string,
  onRef: func,
  placeholderTextColor: string,
  inputStyle: TextStyle
};
class TextInputComponent extends Component<Props> {
  state={
    focus: false
  }
  _onFocus = () => {
    this.setState({focus: true})
  }
  _onSubmitEditing = () => {
    this.props.onSubmitEditing ? this.props.onSubmitEditing() : null 
    this.setState({focus: false})
  }
  render(){
  let height = 50 
  return(
    <View style={[{flexDirection: 'row',width : '100%', height: height, backgroundColor:'#ede9e8'}, this.props.conteinerStyle]}>
      {
        this.props.iconName ? (
          <View style={{width: height, height: height, justifyContent: 'center', alignItems: 'center'}}>
            <Ionicons name={this.props.iconName} size={30} color={this.props.iconColor ? this.props.iconColor : (this.state.focus ? this.props.activeIconColor: this.props.inactiveIconColor)}/>
          </View> 
        ):null
      }
      <TextInput
        ref = {this.props.onRef}
        style={[{flex:1, fontSize: 17,marginRight: 15, marginLeft: this.props.iconName ? 0:15},this.props.inputStyle]}
        underlineColorAndroid={'transparent'}
        defaultValue={this.props.defaultValue}
        value={this.props.value}
        placeholder={this.props.placeholder}
        autoCorrect={this.props.autoCorrect}
        autoFocus={this.props.autoFocus}
        editable={typeof this.props.editable == 'undefined'? true : this.props.editable}
        keyboardType={this.props.keyboardType}
        maxLength={this.props.maxLength}
        multiline={this.props.multiline}
        onSubmitEditing={this.props.onSubmitEditing}
        onChangeText={this.props.onChangeText}
        onFocus={this.props.onFocus ? this.props.onFocus : this._onFocus}
        onBlur={this.props.onBlur ? this.props.onBlur:this._onSubmitEditing}
        secureTextEntry={this.props.secureTextEntry}
        selectTextOnFocus={this.props.selectTextOnFocus}
        returnKeyType={this.props.returnKeyType}
        placeholderTextColor={this.props.placeholderTextColor}
      />
    </View>
  );
  }
};
export default TextInputComponent;