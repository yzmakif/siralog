import { StyleSheet } from "react-native";

const RankStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    rankContainer: {
        flex: 1
    }
})

export {RankStyle}