import { StyleSheet } from "react-native";

const SearchStyle = StyleSheet.create({
    backViewStyle: {
        flex: 1,
        backgroundColor: '#26d591'
    },
    containerStyle:{
        flex:1,
        backgroundColor:'#fff',
        alignItems:'center',
        justifyContent:'center',
        elevation: 15
    },
    subContainerStyle:{
        width:'100%',
        height:'60%',
    },
    textStyle:{
        fontSize:18,
        color:'black',
        fontWeight:'500',
        marginTop:15
    },
    subTextStyle:{
        fontSize:17,
        color:'black',
        fontWeight:'500',
        
    },
    viewStyle:{
        borderBottomWidth:1,
        borderColor:'black',
    },
    buttonViewStyle:{
        marginBottom:5,
        marginTop:10,
        width:'99%',
        height:60,
        justifyContent:'center',
        alignItems:'center',
        flex:1
    },   
})

export {SearchStyle}