import { StyleSheet } from "react-native";

const OperationStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#fff',
    },
    list: {
        flex: 1,
        backgroundColor: '#fff'
    },
    empty: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    emptyText: {
        fontSize: 20, 
        fontWeight: 'bold', 
        color: '#2e2e2e'
    }
})

export {OperationStyle}