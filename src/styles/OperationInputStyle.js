import { StyleSheet } from "react-native";
import { backgroundColor } from "../constant/AppConstant";

const OperationInputStyle = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
    },
    inputContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    textInput: {
        width: '95%', 
        height: 50, 
        alignSelf:'center', 
        borderRadius: 25,
        marginTop: 10,
        borderWidth: 0.5,
        borderColor: '#2e2e2e',
    },
    ButtonStyle: {
        width: '95%',
        height: 50,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ff4d4d',
        alignSelf: 'center'
    },
    ButtonTextStyle:{
        fontSize: 22,
        color: '#fff'
    },
})
export {OperationInputStyle}