import { StyleSheet} from "react-native";
const SignInStyles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
    },
    containerLogo:{
        width: '100%',
        height: '40%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerInput:{
        width: '100%',
        height: '60%'
    },
    logoStyle:{
        width: '100%',
        height: 80,
        tintColor: '#2e2e2e'
    },
    textInput: {
        width: '90%', 
        height: 50, 
        alignSelf:'center', 
        borderRadius: 25
    },
    loginButtonStyle: {
        width: '90%',
        height: 50,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,100,0,1)',
        alignSelf: 'center'
    },
    loginButtonTextStyle:{
        fontSize: 22,
        color: '#fff'
    },
    bottomContainerStyle:{
        position: 'absolute',
        bottom: 0,
        width: '90%',
        height: 60,
        alignSelf: 'center',
        flexDirection: 'row'
    },
    bottomContainerChildStyle:{
        flex:1,
        alignItems :'center',
        paddingHorizontal: 5
    },
    bottomTextStyle: {
        fontSize: 17,
        color: '#e2e2e2',
        fontWeight: 'bold'
    }
})
export {SignInStyles}