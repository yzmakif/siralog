import { StyleSheet } from "react-native";

const SettingsStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    }
})

export {SettingsStyle}