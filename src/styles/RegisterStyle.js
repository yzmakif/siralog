import { StyleSheet, Dimensions } from "react-native";
const backgroundColor = '#E8E5E5'

const RegisterStyle = StyleSheet.create({
    conteiner:{
        flex: 1
    },
    containerLogo:{
        width: '100%',
        height: Dimensions.get('screen').height*0.4,
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerInput:{
        width: '100%',
        height: 700
    },
    logoStyle:{
        width: '100%',
        height: 80,
        tintColor: '#2e2e2e'
    },
    userInfoConteinerStyle:{
        width: '100%',
        marginBottom: 20
    },
    inputTopStyle:{
        alignSelf: 'center', 
        borderTopLeftRadius: 10, 
        borderTopRightRadius: 10, 
        marginBottom: 3,
        backgroundColor: backgroundColor
    },
    inputBottomStyle: {
        alignSelf: 'center', 
        borderBottomLeftRadius: 10, 
        borderBottomRightRadius: 10, 
        marginBottom: 3,
        backgroundColor: backgroundColor 
    },
    inputCenterStyle: {
        alignSelf: 'center',
        marginBottom: 3,
        backgroundColor: backgroundColor 
    },
    registerButtonStyle: {
        width: '90%',
        height: 50,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,100,0,1)',
        alignSelf: 'center'
    },
    registerButtonTextStyle:{
        fontSize: 22,
        color: '#fff'
    },
    infoTextStyle: {
        textAlign: 'center',
        width: '90%',
        alignSelf: 'center',
        marginTop: 10,
        fontSize: 14
    },
    headerText: {
        color: '#fff',
        fontSize: 18
    }
})

export {RegisterStyle}