import { StyleSheet } from "react-native";

const PasswordChangeStyle = StyleSheet.create({
    container: {
        flex: 1,
    },
    logoConteiner:{
        flex: 2,
        justifyContent :'center',
        alignItems: 'center'
    },
    inputContainer: {
        flex: 3
    },
    loginButtonStyle: {
        width: '90%',
        height: 50,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,100,0,1)',
        alignSelf: 'center'
    },
    loginButtonTextStyle:{
        fontSize: 22,
        color: '#fff'
    },
    textInput: {
        width: '90%', 
        height: 50, 
        alignSelf:'center', 
        borderRadius: 25,
        marginBottom: 20
    },
    logoStyle:{
        width: '100%',
        height: 80,
        tintColor: '#2e2e2e'
    }
})

export {PasswordChangeStyle}