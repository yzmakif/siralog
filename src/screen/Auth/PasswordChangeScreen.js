import React, { Component } from 'react'
import { View, Text, ImageBackground, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

import { PasswordChangeStyle as styles } from "../../styles";
import TextInput from '../../components/Input';

export class PasswordChangeScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            password: '',
            passwordAgain: ''
        }
        this.password = null
        this.passwordAgain = null
    }
    render() {
        return (
            <ImageBackground style={styles.container} source={require('../../images/loginBackground.jpg')}>
                <View style={styles.logoConteiner}>
                    <Image source={require('../../images/logo.png')} style={styles.logoStyle} resizeMode='contain' />
                </View>
                <View style={styles.inputContainer}>
                    <TextInput 
                        iconName='ios-lock'
                        iconColor='gray'
                        placeholder='Yeni Şifre'
                        keyboardType='default'
                        returnKeyType='next'
                        value={this.state.password}
                        conteinerStyle={styles.textInput}
                        maxLength={40}
                        onChangeText={(text) => this.setState({password: text})}
                        onSubmitEditing={() => this.passwordAgain.focus()}
                    />
                    <TextInput 
                        iconName='ios-lock'
                        iconColor='gray'
                        placeholder='Yeni Şifreyi Doğrula'
                        keyboardType='default'
                        returnKeyType='done'
                        value={this.state.passwordAgain}
                        conteinerStyle={styles.textInput}
                        maxLength={40}
                        onChangeText={(text) => this.setState({passwordAgain: text})}
                    />
                    <TouchableOpacity style={styles.loginButtonStyle}>
                        <Text style={styles.loginButtonTextStyle}>Kodu Doğrula</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        
    }
}

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(PasswordChangeScreen)
