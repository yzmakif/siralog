import React, { Component } from 'react'
import { View, Text, ImageBackground, TouchableOpacity, Image } from 'react-native'
import { connect } from 'react-redux'

import { ForgetPasswordStyle as styles } from "../../styles";
import TextInput from "../../components/Input";

export class ForgetPasswordScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            phone: ''
        }
    }
    _forgetPasswordPress = () => {
        this.props.navigation.navigate('VerifyCode')
    }
    render() {
        return (
            <ImageBackground style={styles.container} source={require('../../images/loginBackground.jpg')}>
                <View style={styles.logoConteiner}>
                    <Image source={require('../../images/logo.png')} style={styles.logoStyle} resizeMode='contain' />
                </View>
                <View style={styles.inputContainer}>
                    <TextInput 
                        iconName='ios-lock'
                        iconColor='gray'
                        placeholder='Telefon Numaranız'
                        keyboardType='number-pad'
                        returnKeyType='done'
                        value={this.state.phone}
                        conteinerStyle={styles.textInput}
                        maxLength={10}
                        onChangeText={(text) => this.setState({phone: text})}
                    />
                    <TouchableOpacity onPress={this._forgetPasswordPress} style={[styles.loginButtonStyle,{marginTop: 20}]}>
                        <Text style={styles.loginButtonTextStyle}>Şifre Gönder</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}

const mapStateToProps = () => {
    return {

    }
}

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgetPasswordScreen)