import React, { Component } from 'react'
import { View,ImageBackground, TouchableOpacity, Text, Image, ScrollView, StatusBar} from 'react-native'
import { connect } from 'react-redux'

import { RegisterStyle as styles } from "../../styles";
import TextInputComponent from "../../components/TextInputComponent";
import { registerTextChanged, _registerUser } from "../../actions";

export class RegisterScreen extends Component {
    constructor(props){
        super(props);
        this.tc = null
        this.name = null
        this.surname = null
        this.day = null
        this.mount = null
        this.year = null
        this.phone = null
        this.email = null
        this.password = null
        this.passwordAgain = null
    }
    _registerPress = () => {
        this.props._registerUser(
            this.props.navigation,
            this.props.tc,
            this.props.name,
            this.props.surname,
            this.props.day,
            this.props.mount,
            this.props.year,
            this.props.phone,
            this.props.email,
            this.props.password,
            this.props.passwordAgain
        )
    }
    componentWillUnmount(){
        this.props.registerTextChanged('tc','')
        this.props.registerTextChanged('name','')
        this.props.registerTextChanged('surname','')
        this.props.registerTextChanged('day','')
        this.props.registerTextChanged('mount','')
        this.props.registerTextChanged('year','')
        this.props.registerTextChanged('phone','')
        this.props.registerTextChanged('email','')
        this.props.registerTextChanged('password','')
        this.props.registerTextChanged('passwordAgain','')
    }
    render() {
        return (
        <ImageBackground style={styles.conteiner} source={require('../../images/loginBackground.jpg')}>
            <StatusBar hidden={true} />
            <ScrollView style={{flex: 1}}>
                <View style={styles.containerLogo}>
                    <Image source={require('../../images/logo.png')} style={styles.logoStyle} resizeMode='contain' />
                </View>
                <View style={styles.containerInput}>
                    <View style={styles.userInfoConteinerStyle}>
                        <TextInputComponent
                            onRef={ref => this.tc = ref} 
                            iconName={'ios-person'}
                            placeholder='TC Kimlik No' 
                            width={'90%'} 
                            keyboardType={'numeric'}
                            returnKeyType={'next'}
                            value={this.props.tc}
                            height={50}
                            maxLength={11}
                            style={styles.inputTopStyle}
                            onChangeText={(text) => this.props.registerTextChanged('tc',text)}
                            onSubmitEditing={() => this.name.focus()}
                        />
                        <TextInputComponent 
                            onRef={ref => this.name = ref}
                            iconName={'ios-mail'}
                            placeholder='İsim' 
                            width={'90%'} 
                            keyboardType={'default'}
                            returnKeyType={'next'}
                            value={this.props.name}
                            height={50}
                            maxLength={100}
                            style={styles.inputBottomStyle} 
                            onChangeText={(text) => this.props.registerTextChanged('name',text)}
                            onSubmitEditing={() => this.surname.focus()}
                        />
                        <TextInputComponent 
                            onRef={ref => this.surname = ref}
                            iconName={'ios-mail'}
                            placeholder='Soyisim' 
                            width={'90%'} 
                            keyboardType={'default'}
                            returnKeyType={'next'}
                            value={this.props.surname}
                            height={50}
                            maxLength={100}
                            style={styles.inputBottomStyle} 
                            onChangeText={(text) => this.props.registerTextChanged('surname',text)}
                            onSubmitEditing={() => this.day.focus()}
                        />
                        <TextInputComponent 
                            onRef={ref => this.day = ref}
                            iconName={'ios-mail'}
                            placeholder='Dogum Tarihi(Gün)' 
                            width={'90%'} 
                            keyboardType={'numeric'}
                            returnKeyType={'next'}
                            value={this.props.day}
                            height={50}
                            maxLength={2}
                            style={styles.inputBottomStyle} 
                            onChangeText={(text) => this.props.registerTextChanged('day',text)}
                            onSubmitEditing={() => this.mount.focus()}
                        />
                        <TextInputComponent 
                            onRef={ref => this.mount = ref}
                            iconName={'ios-mail'}
                            placeholder='Dogum Tarihi(Ay)' 
                            width={'90%'} 
                            keyboardType={'numeric'}
                            returnKeyType={'next'}
                            value={this.props.mount}
                            height={50}
                            maxLength={2}
                            style={styles.inputBottomStyle} 
                            onChangeText={(text) => this.props.registerTextChanged('mount',text)}
                            onSubmitEditing={() => this.year.focus()}
                        />
                        <TextInputComponent 
                            onRef={ref => this.year = ref}
                            iconName={'ios-mail'}
                            placeholder='Dogum Tarihi(Yıl)' 
                            width={'90%'} 
                            keyboardType={'numeric'}
                            returnKeyType={'next'}
                            value={this.props.year}
                            height={50}
                            maxLength={4}
                            style={styles.inputBottomStyle} 
                            onChangeText={(text) => this.props.registerTextChanged('year',text)}
                            onSubmitEditing={() => this.phone.focus()}
                        />
                        <TextInputComponent 
                            onRef={ref => this.phone = ref}
                            iconName={'ios-mail'}
                            placeholder='Telefon Numarası' 
                            width={'90%'} 
                            keyboardType={'phone-pad'}
                            returnKeyType={'next'}
                            value={this.props.phone}
                            height={50}
                            maxLength={15}
                            style={styles.inputBottomStyle} 
                            onChangeText={(text) => this.props.registerTextChanged('phone',text)}
                            onSubmitEditing={() => this.email.focus()}
                        />
                        <TextInputComponent 
                            onRef={ref => this.email = ref}
                            iconName={'ios-mail'}
                            placeholder='E-Mail' 
                            width={'90%'} 
                            keyboardType={'default'}
                            returnKeyType={'next'}
                            value={this.props.email}
                            height={50}
                            maxLength={100}
                            style={styles.inputBottomStyle} 
                            onChangeText={(text) => this.props.registerTextChanged('email',text)}
                            onSubmitEditing={() => this.password.focus()}
                        />
                    </View>
                    <View style={styles.userInfoConteinerStyle}>
                        <TextInputComponent
                            onRef={ref => this.password = ref}
                            secureTextEntry={true} 
                            iconName={'ios-mail'}
                            placeholder='Parola' 
                            width={'90%'} 
                            keyboardType={'default'}
                            returnKeyType={'next'}
                            value={this.props.password}
                            height={50}
                            maxLength={100}
                            style={styles.inputCenterStyle} 
                            onChangeText={(text) => this.props.registerTextChanged('password',text)}
                            onSubmitEditing={() => this.passwordAgain.focus()}
                        />
                        <TextInputComponent 
                            onRef={ref => this.passwordAgain = ref}
                            secureTextEntry={true}
                            iconName={'ios-mail'}
                            placeholder='Parola Tekrar' 
                            width={'90%'} 
                            keyboardType={'default'}
                            returnKeyType={'done'}
                            value={this.props.passwordAgain}
                            height={50}
                            maxLength={100}
                            style={styles.inputBottomStyle} 
                            onChangeText={(text) => this.props.registerTextChanged('passwordAgain',text)}
                        />
                        <TouchableOpacity onPress={this._registerPress} style={[styles.registerButtonStyle,{marginTop: 20}]}>
                            <Text style={styles.registerButtonTextStyle}>Kayıt Ol</Text>
                        </TouchableOpacity>
                        <Text style={styles.infoTextStyle}>Kayıt olarak Kullanım Şartları ve Gizlilik Politikasını kabul etmiş olursunuz</Text>
                    </View>
                </View>
            </ScrollView>
        </ImageBackground>
        )
    }
}

const mapStateToProps = ({registerResponse}) => {
    const {
        tc,
        name,
        surname,
        day,
        mount,
        year,
        phone,
        email,
        password,
        passwordAgain
    } = registerResponse
    return{
        tc,
        name,
        surname,
        day,
        mount,
        year,
        phone,
        email,
        password,
        passwordAgain
    }
}

const mapDispatchToProps = {
    registerTextChanged,
    _registerUser
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)
