import React from "react"
import { View, StatusBar, TouchableOpacity, Text, ImageBackground, Image} from "react-native"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { _signInAsync, loginTextChanged} from "../../actions"
import { SignInStyles as styles } from "../../styles";
import TextInput from "../../components/Input";

class SignInScreen extends React.Component {
  constructor(props) {
    super(props)
    this.tc = null
    this.password = null
  }
  _loginPress = () => {
    this.props._signInAsync(this.props.navigation, this.props.tc, this.props.password)
  }
  _registerPress = () => {
    this.props.navigation.navigate('Register')
  }
  _forgetPasswordPress = () => {
    this.props.navigation.navigate('ForgetPassword')
  }
  componentWillUnmount(){
    this.props.loginTextChanged('tc', '');
    this.props.loginTextChanged('password', '')
  }
  render() {
    return (
      <ImageBackground style={styles.container} source={require('../../images/loginBackground.jpg')}>
        <StatusBar hidden={true} />
        <View style={styles.containerLogo}>
          <Image source={require('../../images/logo.png')} style={styles.logoStyle} resizeMode='contain' />
        </View>
        <View style={styles.containerInput}>
          <TextInput 
            onRef={ref => this.tc = ref}
            iconName='ios-person'
            iconColor='gray'
            placeholder='TC Kimlik Numarası'
            keyboardType='number-pad'
            returnKeyType='next'
            value={this.props.tc}
            onChangeText={(text) => this.props.loginTextChanged('tc', text)}
            onSubmitEditing={() => {this.password.focus()}}
            maxLength={11}
            conteinerStyle={styles.textInput}
          />
          <TextInput 
            secureTextEntry
            onRef={ref => this.password = ref}
            iconName='ios-lock'
            iconColor='gray'
            placeholder='Parola'
            keyboardType='default'
            returnKeyType='done'
            value={this.props.password}
            onChangeText={(text) => this.props.loginTextChanged('password', text)}
            conteinerStyle={[styles.textInput,{marginTop: 20}]}
          />
          <TouchableOpacity onPress={this._loginPress} style={[styles.loginButtonStyle,{marginTop: 20}]}>
            <Text style={styles.loginButtonTextStyle}>Giriş Yap</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.bottomContainerStyle}>
          <TouchableOpacity onPress={this._registerPress} style={[styles.bottomContainerChildStyle,{flexDirection: 'row'}]}>
            <Text style={styles.bottomTextStyle} >Kayıt Ol</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._forgetPasswordPress} style={[styles.bottomContainerChildStyle,{flexDirection: 'row-reverse'}]}>
            <Text style={styles.bottomTextStyle}>Şifremi Unuttum</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    )
  }
}
SignInScreen.proptypes = {
  _signInAsync: PropTypes.func
}
const mapStateToProps = ({singInResponse}) => {
  const {tc, password} = singInResponse
  return{
    tc,
    password
  }
}
export default connect(mapStateToProps,{
  _signInAsync,
  loginTextChanged
})(SignInScreen)