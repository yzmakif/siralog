import React, { Component } from 'react'
import { View, Text, ImageBackground, Alert, TouchableOpacity  } from 'react-native'
import { connect } from 'react-redux'
import { AnimatedCircularProgress } from 'react-native-circular-progress';


import TextInput from "../../components/Input";
import { VerifyCodeStyle as styles } from "../../styles";

MAX_POINTS = 250
export class VerifyCodeScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            code: '',
            fill: 10
        }
    }
    componentDidMount(){
        this._timeStart()
    }
    _timeStart = () => {
        this.sayac =  setInterval(() => {
            this.setState({fill: this.state.fill-1})
            if(this.state.fill == 0){
                this._timeEnd()
            }
        },1000)
    }
    _timeEnd = () => {
        clearInterval(this.sayac);
        Alert.alert('Süre Bitti', 'Size verilen süre içerisinde işlem yapmadınız.', [
            {text: 'İşlemi İptal Et', onPress: () => {
                //Ana Sayfaya At
            }},
            {text: 'Yeni Kod Gönder',onPress: () => {
                this.setState({fill: 250})
                this._timeStart()
            }},
        ])
    }
    _verifyPress = () => {
        this.props.navigation.navigate('PasswordChange')
    }
    render() {
        const fill = this.state.fill / MAX_POINTS * 100;
        return (
            <ImageBackground style={styles.container} source={require('../../images/loginBackground.jpg')}>
                <View style={styles.counterConteiner}>
                <AnimatedCircularProgress
                    size={200}
                    width={15}
                    fill={fill}
                    tintColor="#00e0ff"
                    prefill={0}
                    rotation ={-360}
                    backgroundColor="#3d5875"
                    
                >
                {
                    (fill) => (
                    <Text style={styles.points}>
                        { this.state.fill }
                    </Text>
                    )
                }
                </AnimatedCircularProgress>
                </View>
                <View style={styles.inputContainer}>
                    <TextInput 
                        iconName='ios-lock'
                        iconColor='gray'
                        placeholder='Telefonunuza gelen kodu giriniz'
                        keyboardType='number-pad'
                        returnKeyType='done'
                        value={this.state.code}
                        conteinerStyle={styles.textInput}
                        maxLength={6}
                        onChangeText={(text) => this.setState({code: text})}
                    />
                    <TouchableOpacity onPress={this._verifyPress} style={[styles.loginButtonStyle,{marginTop: 20}]}>
                        <Text style={styles.loginButtonTextStyle}>Kodu Doğrula</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}

const mapStateToProps = () => {
    return {

    }
}

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(VerifyCodeScreen)