import React from "react";
import {
  Animated,
  Dimensions,
  PanResponder,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

const { height } = Dimensions.get("window");

const fontColour = "#07080A";

const styles = StyleSheet.create({
  flex: {
    flex: 1
  },
  container: {
    height: 400,
    alignSelf: "stretch"
  },
  ticketTopContainer: {
    flex: 1,
    paddingTop: 16,
    paddingHorizontal: 16,
    backgroundColor: "#FFFFFF",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    borderColor: "#EEEEEE",
    borderBottomWidth: 1
  },
  ticketBottomContainer: {
    flex: 2
  },
  queueSummaryContainer: {
    flex: 1,
    paddingTop: 32,
    paddingLeft: 16,
    paddingRight: 24,
    backgroundColor: "#FFFFFF",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  acceptTicketContainer: {
    flex: 1,
    paddingHorizontal: 16,
    backgroundColor: "#9AD275",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  heading: {
    fontSize: 28,
    fontWeight: "bold"
  },
  subHeading: {
    fontSize: 16,
    fontWeight: "100"
  },
  ticketSummary: {
    // Used to offset the big ticketNumber size
    paddingTop: 16
  },
  ticketNumber: {
    fontSize: 70,
    textAlign: "right"
  },
  queueHeader: {
    paddingBottom: 4
  },
  queueValue: {
    fontSize: 28,
    fontWeight: "bold",
    color: "#333333"
  },
  greyText: {
    color: "#07080A"
  },
  lightGreyText: {
    color: "#AAAAAA"
  },
  whiteText: {
    color: "#FFFFFF"
  },
  notchContainer: {
    position: "absolute",
    width: "100%",
    flexDirection: "row",
    justifyContent: "center"
  },
  notch: {
    height: 80,
    width: 80,
    borderRadius: 80
  },
  ticketNotchContainer: {
    bottom: -60
  },
  ticketNotch: {
    backgroundColor: "#00B9F1"
  }
});

class Ticket extends React.Component {
  constructor(props) {
    super(props);

    this.ticketAcceptedAnimation = new Animated.Value(0);
    this.ticketHidingAnimation = new Animated.Value(0);

    this.ticketDragPanResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderGrant: (e, gesture) => {
        if (!this.state.ticketTaken) {
          this.setState({ ticketTaken: true}, () => {
            Animated.timing(this.ticketAcceptedAnimation, {
              toValue: 1,
              duration: 500,
              useNativeDriver: true
            }).start(() => {
              this.setState({ticketTaken: true})
              /* Animated.timing(this.ticketHidingAnimation, {
                toValue: 1,
                duration: 1000,
                delay: 4500,
                useNativeDriver: true
              }).start(() => {
                //this.props.onTicketTaken();
              }); */
            });
          });
        }
      }
    });

    this.state = {
      ticketTaken: false,
    };
  }

  render() {
    const {
      ticketNumber,
      ticketDate,
      ticketTime,
      estimatedWaitTime,
      queuePosition
    } = this.props;

    const ticketStyles = {
      transform: [
        {
          translateY: this.ticketAcceptedAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 100]
          })
        },
        {
          scale: this.ticketHidingAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0]
          })
        }
      ],
      opacity: this.ticketHidingAnimation.interpolate({
        inputRange: [0, 1],
        outputRange: [1, 0]
      })
    };

    return (
      <View style={{ height: 800 }}>
        <Animated.View style={[styles.container, ticketStyles]}>
          <Animated.View style={[styles.ticketTopContainer]}>
            <View style={[styles.flex, styles.ticketSummary]}>
              <Text style={[styles.heading, styles.greyText]}>
                Sıranız
              </Text>
              <Text style={[styles.subHeading, styles.lightGreyText]}>
                {ticketDate}
              </Text>
              <Text style={[styles.subHeading, styles.lightGreyText]}>
                {ticketTime}
              </Text>
            </View>
            <View style={styles.flex}>
              <Text
                style={[styles.heading, styles.ticketNumber, styles.greyText]}
              >
                {ticketNumber}
              </Text>
            </View>
          </Animated.View>

          <View style={styles.ticketBottomContainer}>
            <View style={styles.queueSummaryContainer}>
              <View>
                <Text
                  style={[
                    styles.subHeading,
                    styles.greyText,
                    styles.queueHeader
                  ]}
                >
                  Önünüzde
                </Text>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <View style={{ paddingRight: 8 }}>
                    <Icon name="md-people" color="#000000" size={32} />
                  </View>
                  <View>
                    <Text style={styles.queueValue}>{queuePosition}</Text>
                  </View>
                </View>
              </View>
              <View>
                <Text
                  style={[
                    styles.subHeading,
                    styles.greyText,
                    styles.queueHeader,
                  ]}
                >
                  İşlemde
                </Text>

                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <View style={{ paddingRight: 8 }}>
                    <Icon name="md-people" color="#000000" size={32} />
                  </View>
                  <View>
                    <Text style={styles.queueValue}>
                      {estimatedWaitTime}{" "}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <View
              {...this.ticketDragPanResponder.panHandlers}
              style={[styles.acceptTicketContainer,{
                      backgroundColor: queuePosition >0 ? '#9AD275':'gray'
                    }]}
            >
              <View>
                <Text numberOfLines={1} style={[styles.heading, styles.whiteText]}>
                  {this.props.companyName}
                </Text>
                <Text style={[styles.subHeading, styles.whiteText]}>
                  {this.props.operationName}
                </Text>
              </View>
              <View style={{ paddingRight: 24 }}>
              </View>
            </View>
          </View>

          <View style={[styles.notchContainer, styles.ticketNotchContainer]}>
            <View style={[styles.notch, styles.ticketNotch]} />
          </View>
        </Animated.View>
      </View>
    );
  }
}

export default Ticket;
