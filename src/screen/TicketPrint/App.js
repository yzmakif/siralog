import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import { connect } from "react-redux";

import { clearRank } from "../../actions";
import Printer from "./Printer";
import Ticket from "./Ticket";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 32,
    paddingHorizontal: 32,
    backgroundColor: "#00B9F1"
  }
});
class TicketPrint extends Component {
  state = {
    // This is a lazy way to rerender the printer and the ticket to
    // restart the animation by rendering everything
    ticketIndex: 1
  };

  componentWillUnmount(){
    this.props.clearRank();
  }
  componentWillMount(){
    console.log('newRank : ',this.props.newRank);
    
  }
  componentDidMount(){
    let d = new Date().toISOString().slice(0,10);
    console.log('date : ',d);
    
  }
  render() {
    const ticketHeight = 400;
    let queuePosition, date, time, rank, activeNumber,companyName, operationName;
    if(this.props.screen == 'rank'){
      queuePosition = this.props.rank.number - this.props.rank.activeNumber
      date = this.props.rank.date.slice(0,10)
      time = this.props.rank.date.slice(11,16)
      rank = this.props.rank.number
      activeNumber = this.props.rank.activeNumber
      companyName = this.props.rank.companyName
      operationName = this.props.rank.operationName
    }else if(this.props.screen == 'newRank'){
      queuePosition = this.props.newRank.Rank - this.props.newRank.ActiveNumber
      date = this.props.newRank.RankDate.slice(0,10)
      time = this.props.newRank.RankDate.slice(11,16)
      rank = this.props.newRank.Rank
      activeNumber = this.props.newRank.ActiveNumber
      companyName=this.props.newRank.CompanyName
      operationName = this.props.newRank.operationName
    }
    return (
      <View style={styles.container}>
        <Printer key={this.state.ticketIndex} ticketHeight={ticketHeight}>
          <Ticket
            height={ticketHeight}
            ticketNumber={JSON.stringify(rank)}
            ticketDate={date}
            ticketTime={time}
            estimatedWaitTime={activeNumber > rank ? JSON.stringify(rank):JSON.stringify(activeNumber)}
            queuePosition={queuePosition < 0 ? '0' : JSON.stringify(queuePosition)}
            onTicketTaken={() => {
              this.setState({ ticketIndex: this.state.ticketIndex + 1 });
            }}
            companyName={companyName}
            operationName = {operationName}
          />
        </Printer>
      </View>
    );
  }
}

const mapStateToProps = ({operationResponse}) => {
  const { newRank,rank, screen } = operationResponse;
  return {
    newRank,
    rank,
    screen
  }
}
export default connect(mapStateToProps,{
  clearRank
})(TicketPrint);
