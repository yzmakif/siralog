import React, {Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    Image,
    Animated,
    TouchableOpacity,
    StatusBar
} from 'react-native';
import { connect } from 'react-redux';
import { pushData, getCompanyData, pushCompanyId } from '../../actions';
import Header from "../../components/Header";
import { SearchStyle as styles } from "../../styles";
import emojiScan from '../../images/emoji.png';
import boyTara from '../../images/boy.png';
import girlTara from '../../images/girl.png';
import detectiveGirl from '../../images/detectiveGirl.png';
import detectiveBoy from '../../images/detectiveBoy.png'
import CompanyListItem from "../../components/CompanyListItem";

import { backgroundColor } from "../../constant/AppConstant";

class SearchScreen extends Component {

    //Tasarımda kullanılan resimlerin rastgele gelmesi için içinden seçim yapacağımız arrayler.
    state = {
        imgArray: [boyTara, girlTara],
        imgDetectiveArray: [detectiveGirl, detectiveBoy]
    }

    //Büyüteç animasyonu için gerekli bileşen
    constructor() {
        super()
        //Animasyon için değeri belirledim
        this.springValue = new Animated.Value(0)
    }

    componentDidMount() {
        //Animasyonu çağırdım
        this.spring();
        this.pushData();
    }

    //Animasyon fonksiyonu
    spring = () => {
        this.springValue.setValue(0)
        Animated.spring(
          this.springValue,
          {
            toValue: 1,
            friction: 0.5,
          }
        ).start(() => this.spring())
    }

    //İçeriği render eden fonksiyon
    renderData(){
        //Gelen kurumların listedinin uzunlupu 0'dan büyükse yani doluysa ve tareama yapılmıyorsa render edilen kısım
        if (this.props.companies.length > 0 && this.props.scanning == false){
            return (
                <ScrollView contentContainerStyle = {{alignItems: 'center'}} style={styles.subContainerStyle}>
                    {
                        //Reducerdan propsa gelen companies'i mapliyoruz ve companyName ile ilgili logoyu tasarıma ekliyoruz.
                        this.props.companies.map((data, index)=>{
                            return <CompanyListItem key={index} name={data.name} item={data} navigation={this.props.navigation} />
                        })
                    }
                </ScrollView>
            )    
        }
         //Gelen kurumların listedinin uzunlupu 0'dan büyükse veya eşitsde yani doluysa ve tareama yapılıyorsa render edilen kısım
        else if(this.props.scanning == true && this.props.companies.length >= 0 ) {
            const spring = this.springValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1]
          });
            return(
                <View style = {{flex:1,justifyContent: 'center', alignItems: 'center'}}>
                    <Animated.Image 
                    style={{
                        transform: [{scale: spring}],
                        height: 150,
                        width: 150
                    }}
                    source={emojiScan}
                    />    
                </View>
            );
        }
         //Gelen kurumların listedinin uzunlupu 0' a eşitse  ve tareama yapılmıyorsa render edilen kısım
        else if(this.props.scanning == false && this.props.companies.length == 0 ){
            //alert(image);
            return(
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <View style = {{flex: 3, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={[styles.subTextStyle,{textAlign:'center'}]}>Çevrenizde kurum bulunamadı.</Text>
                    <Text style={[styles.subTextStyle,{textAlign:'center'}]}> Çevrenizdeki kurumlara erişmek için</Text>
                    <Text style={[styles.subTextStyle,{textAlign:'center'}]}>  lütfen 
                        <Text style={{color:'#ff4d4d',fontSize:20,fontWeight:'900'}}>
                            {' '+'TARA'+' '}
                        </Text>
                        butonuna basın.
                    </Text>
                </View>
                <View style = {{flex:1,marginBottom: 25}}>
                    <Image source= {this.state.imgDetectiveArray[Math.floor(Math.random() * this.state.imgDetectiveArray.length)]}/>
                </View>
                </View>
            );

        }    
    }

    pushCompanyId = (param) => {
        this.props.pushCompanyId(param);
    }

    getCompanyData = () => {
        this.props.getCompanyData();
    }

    pushData = () => {
        this.props.pushData(this.props.user.id); 
    }
    render(){
        this.getCompanyData();
        return(
            <View style={styles.containerStyle}>
                <StatusBar animated={true} backgroundColor={backgroundColor} />
                <Header 
                    backgroundColor='#2e2e2e'
                    title='İşletmeler'
                    leftPress={() => {
                        this.props.navigation.goBack()
                    }}
                />
                <View style={{flex:4, width:'100%'}}>
                    {this.renderData()}    
                </View>
                <View style={styles.buttonViewStyle}>
                    <TouchableOpacity onPress = {this.props.scanning ? null:this.pushData}><Text style={{fontSize:25,color:'#ff4d4d',fontWeight:'500'}}>{this.props.scanning ? 'TARANIYOR' : 'TARA' }</Text></TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapStateToProps = ({ searchResponse, singInResponse }) => {    
    const { scanning,companies } = searchResponse;
    const { user } = singInResponse;    
    return {
        scanning,
        companies,
        user
    };
};


export default connect(mapStateToProps, { pushData, getCompanyData, pushCompanyId })(SearchScreen);