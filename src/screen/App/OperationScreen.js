import React, { Component } from 'react'
import { View, Text, FlatList, StatusBar } from 'react-native'
import { connect } from 'react-redux'
import Ionicons from "react-native-vector-icons/Ionicons";
const jsonQuery = require('json-query')

import { OperationStyle as styles } from "../../styles";
import { operationProps } from "../../actions";
import OperationListItem from "../../components/OperationListItem";
import Header from "../../components/Header";

import { backgroundColor } from "../../constant/AppConstant";

export class OparationScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            operations: []
        }
    }
    filterOperations = async () => {
        let operations = await jsonQuery(`[*companyId=${this.props.company.id}]`,{
            data: this.props.operations
        }) 
        this.setState({operations: operations.value})
        
    }
    componentWillMount() {
        this.filterOperations();
    }
    componentWillUnmount() {
        this.props.operationProps(null)
    }
    _renderItem = ({item}) => {
        return <OperationListItem item={item} name={item.name} navigation={this.props.navigation} companyId ={this.props.company.id} userId = {this.props.user.id} />
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar animated={true} backgroundColor={backgroundColor} />
                <Header 
                    Left={
                        <Ionicons 
                            name='ios-arrow-back'
                            size={25}
                            color='#fff'
                        />
                    }
                    backgroundColor='#2e2e2e'
                    title={this.props.company.name}
                    leftPress={() => {
                        this.props.navigation.goBack()
                    }}
                />
                <FlatList
                    style={styles.list}
                    data={this.state.operations}
                    renderItem={this._renderItem}
                    ListEmptyComponent={
                        <View style={styles.empty}>
                            <Text style={styles.emptyText}>Operasyon Bulunamadı!</Text>
                        </View>
                    }
                    keyExtractor={(item, index) => item + index}
                />
            </View>
        )
    }
}

const mapStateToProps = ({operationResponse, searchResponse, singInResponse}) => {
    const {company} = operationResponse;    
    const {operations} = searchResponse
    const { user } = singInResponse;   
    return {
        company,
        operations,
        user
    }
}
export default connect(mapStateToProps, {
    operationProps
})(OparationScreen)
