import React, { Component } from 'react'
import { Text, View, StatusBar, SectionList, RefreshControl } from 'react-native'
import { connect } from "react-redux";

import Header from "../../components/Header";
import { backgroundColor } from "../../constant/AppConstant";
import { RankStyle as styles } from "../../styles";
import { getAllRank } from "../../actions";
import RankListHeader from "../../components/RankListHeader";
import RankListItem from "../../components/RankListItem";

export class RankScreen extends Component {
  componentWillMount(){
    this.props.getAllRank(this.props.user.id)
  }
  renderHeader = ({section: {title}}) => {
    return <RankListHeader title={title} />
  }
  renderItem = ({item, index, section}) => {    
    return <RankListItem item={item} navigation={this.props.navigation} />
  }
  _onRefresh = () => {
    this.props.getAllRank(this.props.user.id)
  }
  render() {
    return (
      <View style={styles.container}>
        <StatusBar animated={true} backgroundColor={backgroundColor} />
        <Header 
          backgroundColor='#2e2e2e'
          title='İşlemleriniz'
          leftPress={() => {
              this.props.navigation.goBack()
          }}
        />
        <View style={styles.rankContainer}>
          <SectionList 
            stickySectionHeadersEnabled
            renderItem={this.renderItem}
            renderSectionHeader={this.renderHeader}
            sections={this.props.rank}
            keyExtractor={(item, index) => item + index}
            ListEmptyComponent = {() => {
              return (
                <View style={{width: '99%', height: 50, justifyContent: 'center', paddingLeft: 15}}>
                  <Text style={{fontSize :18, fontWeight: '600', color: '#fff'}}>İşlem Bulunamadı</Text>
                </View>
              )
            }}
            refreshControl={
              <RefreshControl 
                refreshing={this.props.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          />
        </View>
      </View>
    )
  }
}

const mapStateToProps = ({rankResponse, singInResponse}) => {
  const {rank,refreshing} = rankResponse
  const {user} = singInResponse
  return {
    rank,
    user,
    refreshing
  }
}
export default connect(mapStateToProps,{
  getAllRank
})(RankScreen)