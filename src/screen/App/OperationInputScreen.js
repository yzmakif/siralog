import React, { Component } from 'react'
import { View, Text, StatusBar, TouchableOpacity} from 'react-native'
import { connect } from 'react-redux'
import Ionicons from "react-native-vector-icons/Ionicons";

import { OperationInputStyle as styles } from "../../styles";
import TextInput from "../../components/Input";
import Header from "../../components/Header";
import { newRank } from "../../actions";

import { backgroundColor } from "../../constant/AppConstant";

export class OperationInputScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            input:[],
            first: true
        }
    }
    _renderInput = () => {
        return this.props.operation.operationInfo.map((val,i) => {
            this.state.first ? this.state.input.push({name:val.name, value: ''}):null;
            return <TextInput
                        key={i}
                        placeholder={val.name}
                        keyboardType={val.keywordType}
                        conteinerStyle={styles.textInput}
                        value={this.state.input[i].value}
                        onChangeText={text => {this.state.input[i].value = text; this.setState({input: this.state.input}); console.log(this.state.input)}}
                    />; 
        })
    }
    componentDidMount(){
        this.setState({first: false})
    }
    _newRank = () => {
        this.props.newRank(this.props.navigation);
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar animated={true} backgroundColor={backgroundColor} />
                <Header 
                    Left={
                        <Ionicons 
                            name='ios-arrow-back'
                            size={25}
                            color='#fff'
                        />
                    }
                    backgroundColor='#2e2e2e'
                    title={this.props.operation.name}
                    leftPress={() => {
                        this.props.navigation.goBack()
                    }}
                />
                <View style={styles.inputContainer} >
                    {
                        this._renderInput()
                    }
                    <TouchableOpacity onPress={this._newRank} style={[styles.ButtonStyle,{marginTop: 20}]}>
                        <Text style={styles.ButtonTextStyle}>Sıra Al</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const mapStateToProps = ({operationResponse}) => {
    const {operation} = operationResponse;    
    return {
        operation
    }
}

const mapDispatchToProps = {
    newRank
}

export default connect(mapStateToProps, mapDispatchToProps)(OperationInputScreen)
