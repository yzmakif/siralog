import React from "react";
import { View, Button, StatusBar } from "react-native";
import { connect } from "react-redux";

import { _signOutAsync } from "../../actions";
import Header from "../../components/Header";

import { backgroundColor } from "../../constant/AppConstant";
import { SettingsStyle as styles } from "../../styles";

class SettingsScreen extends React.Component {
    _logOutPress = () => {
      this.props._signOutAsync(this.props.navigation)
    }
    render() {
      return (
        <View style={styles.container}>
          <StatusBar animated={true} backgroundColor={backgroundColor} />
          <Header 
            backgroundColor='#2e2e2e'
            title='Ayarlar'
            leftPress={() => {
                this.props.navigation.goBack()
            }}
          />
          <Button title="Çıkış Yap" onPress={this._logOutPress} />
        </View>
      );
    }
    
}


const mapStateToProps = () => {
  return {

  }
}
export default connect(mapStateToProps,{
  _signOutAsync
})(SettingsScreen);