import React from "react";
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  View,
} from 'react-native';
import { connect } from "react-redux";

import { successUser } from "../../actions";
class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken');
    this.props.successUser(userToken);
    this.props.navigation.navigate(userToken ? 'App' : 'Auth');
  };

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1
  }
}
const mapStateToProps = () => {
  return {

  }
}
export default connect(mapStateToProps,{
  successUser
})(AuthLoadingScreen);