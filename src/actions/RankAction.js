import { 
    ALL_RANK,
    REFRESH_START,
    REFRESH_STOP
} from "./type";
import { Alert } from "react-native";
import Axios from "axios";

import { apiHost,apiPort } from "../constant/AppConstant";

export const getAllRank = (userId) => {
    return(dispatch) => {
        dispatch({
            type: REFRESH_START,
        })
        Axios.post(
            `http://${apiHost}:${apiPort}/user/getAllRank`,
            {
                userId: userId
            }
        )
        .then((res) => {
            dispatch({
                type: REFRESH_STOP
            })
            if(res.data.status == 1){
                dispatch({
                    type: ALL_RANK,
                    payload: [
                        {title: 'Aktif İşlemler', data: res.data.activeRank},
                        {title: 'Geçmiş İşlemler', data: res.data.inactiveRank}
                    ]
                })
            }else {
                Alert.alert(
                    'İşlem Başarısız',
                    'Sıra kaydınız alınamadı',[
                        {text: 'Tamam'}
                    ]
                )
            }
        })
        .catch(() => {
            dispatch({
                type: REFRESH_STOP
            })
            Alert.alert(
                'İşlem Başarısız',
                'Sıra kaydınız alınamadı',[
                    {text: 'Tamam'}
                ]
            )
        })
    }
}