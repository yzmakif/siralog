import { PUSH_DATA, SCAN_START, SCAN_STOP, GET_BEACON_DATA, PUSH_COMPANY_ID } from './type';
import BleManager from 'react-native-ble-connect';
import Axios from 'axios';
import { apiHost, apiPort } from "../constant/AppConstant";
//import socket
//BleManager modülü kullanılmıştır.Detaylı bilgiler github sayfasından alınabilir

const startModule = (dispatch) => {
    //TaMOdülü başlatan fonksiyon
    BleManager.start()
    .then(() => {
    });
}

const startScan = (dispatch) => {
    //Taramayuı başlatan fonksiyon
    BleManager.scan([], 12)
    .then(() => {
        //Taramanın başladığını reducera haber veriyoruz.
        dispatch({
            type: SCAN_START
        });
    });
}

const stopScan = (dispatch) => {
    //Taramayı durduracak fonksiyon
    BleManager.stopScan()
    .then(() => {
        //Taramanıon durduğunu reducers haber veriyoruz
        dispatch({
            type: SCAN_STOP
        });
    });
} 

const getArray = (dispatch, userId) => {
    //Tarama sonucu taranan öğrelerin alındığı fonksiyon
    let beaconArray = '';
    BleManager.getDiscoveredPeripherals([])
        .then(async(peripheralsArray) => {
            await peripheralsArray.map((res, key) => {
                //Tanımladığımız boş arraye gelen arrayş mapleyip içindeki değerlerin idsini koyuyuoruz.
                beaconArray += `${res.id},`; 
            });
            
            dispatch({
                type: PUSH_DATA
            });
            Axios.post(
                `http://${apiHost}:${apiPort}/company/searchCompany`,
                {
                    userId: `${userId}`,
                    beacons : beaconArray
                }
            )
            .then((res) => {
                if(res.data.status == 1){
                    dispatch({
                        type: GET_BEACON_DATA,
                        payload: {companies: res.data.companies, operations: res.data.operations}
                    });
                }else{
                    dispatch({
                        type: GET_BEACON_DATA,
                        payload: {companies: [], operations: []}
                    });
                }
            })
            .catch((err)=> {
                console.log('err : ',err);
                dispatch({
                    type: GET_BEACON_DATA,
                    payload: []
                });
            })
        });
}


export const pushData = (userId) => {
    //Beacon gönderen fonksiyon
    return(dispatch) => {
        startModule(dispatch);
        startScan(dispatch);
        setTimeout(() => {
            //Tarama süresini 12 saniye olarak belirlediğimiz için 12 saniye sonrasına zamanlayıcı koyuyoruz ve taramayı durdurup
            //Verileri sertvise atan fonsksiyonu da 12 saniye sonra çağırıyoruz.
            stopScan(dispatch); 
            getArray(dispatch, userId);
        }
       ,12000
        );
    }
}

export const getCompanyData = () => {
    //Gönderilen beaconlara dönen sonucu alan fonksiyon
    return (dispatch) => {
        /* socket.on('companyPhone', (res) => {
            
        }); */
    }     
}

export const pushCompanyId = (param) => {
    //Tıklanan şirketin companyId'sini servise gönderiyoruz.
    return (dispatch) => {
        //socket.emit('transactionsPhone', param);
            dispatch({
                type: PUSH_COMPANY_ID
            });
    }
}
