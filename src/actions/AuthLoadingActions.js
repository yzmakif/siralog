import { 
    SUCCESS_USER
} from "./type";

export const successUser = (user) => {
    return(dispatch) => {
        dispatch({
            type: SUCCESS_USER,
            payload: JSON.parse(user)
        })
    }
}