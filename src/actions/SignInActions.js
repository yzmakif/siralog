import { AsyncStorage, Alert } from "react-native";
import { 
    LOGIN_TEXT_CHANGE,
    SUCCESS_USER,
} from "./type";
import Axios from "axios";
import { apiHost, apiPort } from "../constant/AppConstant";

export const _signInAsync = (navigation, tc, password) => {//Giriş yap servise gidip gelecek
    return (dispatch) => {
        if(tc.length == 11 && password.length >= 6){
            Axios.post(
                `http://${apiHost}:${apiPort}/user/loginUser`,
                {
                    tc: tc,
                    password: password
                }
            )
            .then( async(res) => {
                if(res.data.status == 1){
                    await AsyncStorage.setItem('userToken', JSON.stringify(res.data.user));
                    dispatch({
                        type: SUCCESS_USER,
                        payload: res.data.user
                    })
                    navigation.navigate('App');
                }else {
                    Alert.alert('Giriş Başarısız!','Tc Numarasi veya Parola Hatalı',[
                        {text: 'Tamam'}
                    ]);
                }
            }).catch((err) => {
                console.log(err);
            })
        }else{
            Alert.alert('Giriş Başarısız!','Tc Numarasi veya Parola uygun değil. Tc 11 karakter, Parola en az 6 karakter olmalı',[
                {text: 'Tamam'}
            ]);
        }
    }
};
export const _signOutAsync = (navigation) => {
    return async (dispatch) => {
        await AsyncStorage.clear();
        navigation.navigate('Auth');
    }
};
export const loginTextChanged = (props, value) =>{
    return(dispatch) => {
        dispatch({
            type: LOGIN_TEXT_CHANGE,
            payload: {props,value}
        })
    }
}