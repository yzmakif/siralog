export const LOGIN_TEXT_CHANGE = 'login_text_change';
export const REGISTER_TEXT_CHANGE = 'register_text_change';

export const PUSH_DATA = 'push_data';
export const SCAN_START = 'scan_start';
export const SCAN_STOP = 'scan_stop';
export const GET_BEACON_DATA = 'get_beacon_data';
export const PUSH_COMPANY_ID = 'push_company_id';

export const OPERATION_PROPS = 'operation_props';
export const ACTIVE_OPERATION_PROPS = 'active_operation_props';

export const SUCCESS_USER = 'success_user';

export const INCREASE_RANK = 'increase_rank';
export const CLEAR_RANK = 'clear_rank';

export const ALL_RANK = 'all_rank';
export const RANK = 'rank';
export const SCREEN_PROPS = 'screen_props';
export const REFRESH_START = 'refresh_start';
export const REFRESH_STOP = 'refresh_stop';
