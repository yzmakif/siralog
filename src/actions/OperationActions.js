import { 
    OPERATION_PROPS,
    ACTIVE_OPERATION_PROPS,
    INCREASE_RANK,
    CLEAR_RANK,
    RANK,
    SCREEN_PROPS
} from "./type";
import { Alert } from "react-native";
import Axios from "axios";
import { apiHost,apiPort } from "../constant/AppConstant";
export const operationProps = (company) => {
    return(dispatch) => {
        dispatch({
            type: OPERATION_PROPS,
            payload: company
        });
    }
}
export const activeOperationProps = (operation) => {
    return(dispatch) => {
        dispatch({
            type: ACTIVE_OPERATION_PROPS,
            payload: operation
        });
    }
}
export const newRank = (navigation,userId, companyId, operationId) => {
    return(dispatch) => {
        Axios.post(
            `http://${apiHost}:${apiPort}/queue/addQueue`,
            {
                userId: userId,
                companyId: companyId,
                operationId: operationId
            }
        )
        .then((res) => {
            if(res.data.status == 1){
                dispatch({
                    type: INCREASE_RANK,
                    payload: res.data.rank
                })
                navigation.navigate('Ticket')
            }else{
                Alert.alert(
                    'İşlem Başarısız',
                    'Sıra alma İşlemi Başarısız.',[
                        {text: 'Tamam'}
                    ]
                )
            }
        })
        .catch((err) => {
            Alert.alert(
                'İşlem Başarısız',
                'Sıra alma İşlemi Başarısız.',[
                    {text: 'Tamam'}
                ]
            )
        })
    }
}
export const clearRank = () => {
    return(dispatch) => {
        dispatch({
            type: CLEAR_RANK,
        })
    }
}
export const changeRank = (rank) => {
    return(dispatch) => {
        dispatch({
            type: RANK,
            payload: rank
        })
    }
}
export const screenProps = (screen) => {
    return(dispatch) => {
        dispatch({
            type: SCREEN_PROPS,
            payload: screen
        })
    }
}