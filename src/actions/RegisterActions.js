import { AsyncStorage, Alert } from "react-native";
import { 
    REGISTER_TEXT_CHANGE,
} from "./type";
import Axios from "axios";

import { apiHost, apiPort } from "../constant/AppConstant";

export const registerTextChanged = (props, value) =>{
    return(dispatch) => {
        dispatch({
            type: REGISTER_TEXT_CHANGE,
            payload: {props,value}
        })
    }
}
export const _registerUser = (
    navigation,
    tc,
    name,
    surname,
    day,
    mount,
    year,
    phone,
    email,
    password,
    passwordAgain
) => {//Giriş yap servise gidip gelecek
    return (dispatch) => {
        try {
            if(
                tc.length > 0 &&
                name.length > 0 &&
                surname.length > 0 &&
                day.length > 0 &&
                mount.length > 0 &&
                year.length > 0 &&
                phone.length >0 &&
                email.length > 0 &&
                password.length > 0 &&
                passwordAgain.length > 0
            ){
                let gun, ay, yil;
                gun = Number.parseInt(day)
                ay = Number.parseInt(mount)
                yil = Number.parseInt(year)
                if(tc.length != 11){
                    Alert.alert('İşlem Başarısız','Tc Kimlik Numarsı 11 karakter olmalı',[{text: 'Tamam'}])
                }else if(gun >31 || gun < 1){
                    Alert.alert('İşlem Başarısız','Dogum Tarihi(Gün) aralık dışında',[{text: 'Tamam'}])
                }else if(ay >12 || ay < 1 ){
                    Alert.alert('İşlem Başarısız','Dogum Tarihi(Ay) aralık dışında',[{text: 'Tamam'}])
                }else if(yil > 3000 ||yil < 1900){
                    Alert.alert('İşlem Başarısız','Dogum Tarihi(Yıl) aralık dışında',[{text: 'Tamam'}])
                }else if(phone.length <= 10 && phone.length >= 15){
                    Alert.alert('İşlem Başarısız','Telefon Numarası aralık dışında',[{text: 'Tamam'}])
                }else if(password.length <6){
                    Alert.alert('İşlem Başarısız','Parola en az 6 karakter olmalı',[{text: 'Tamam'}])
                }
                else{
                    if(password == passwordAgain){
                        Axios.post(
                            `http://${apiHost}:${apiPort}/user/add`,
                            {
                                tc: tc,
                                name: name,
                                surname: surname,
                                day: day,
                                mount: mount,
                                year: year,
                                phone: phone,
                                email: email,
                                password: password
                            }
                        )
                        .then( async(res) => {
                            if(res.data.status == 1){
                                await AsyncStorage.setItem('userToken', JSON.stringify(res.data.user));
                                navigation.navigate('App');           
                            }else{
                                Alert.alert(
                                    'İşlem Başarısız',
                                    'Kayıt sırasında bir hata olustu. Lütfen Bütün Alanları kontrol edip tekrar deneyin.',[
                                        {text: 'Tamam'}
                                    ]
                                )
                            }
                        })
                        .catch((err) => {
                            Alert.alert(
                                'İşlem Başarısız',
                                'Kayıt sırasında bir hata olustu. Lütfen Bütün Alanları kontrol edip tekrar deneyin.',[
                                    {text: 'Tamam'}
                                ]
                            )
                        })
                    }else{
                        Alert.alert(
                            'İşlem Başarısız',
                            'Parola ve Parola Tekrar eşleşmiyor.',[
                                {text: 'Tamam'}
                            ]
                        )
                    }
                }
            }else{
                Alert.alert(
                    'İşlem Başarısız',
                    'Bütün alanlar doldurulmalıdır. Eksik alanları kontrol ediniz.',[
                        {text: 'Tamam'}
                    ]
                )
            }
        } catch (error) {
            Alert.alert(
                'İşlem Başarısız',
                'Kayıt sırasında bir hata olustu',[
                    {text: 'Tamam'}
                ]
            )
        }
    }
};